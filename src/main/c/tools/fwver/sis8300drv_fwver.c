/**
 * Struck 8300 Linux userspace library.
 * Copyright (C) 2015 Cosylab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "sis8300drv.h"

#define FW_VER_R_REG    0x500

int main(int argc, char **argv) {
    char            c;
	int             status, verbose;
	unsigned        reg_value;
    unsigned        fw_major, fw_minor, fw_patch;
	sis8300drv_usr  *sisuser;
	
	verbose = 0;
	reg_value = 0;
	
    while ((c = getopt(argc, argv, "hv")) != -1) {
        switch (c) {
            case 'v':
                verbose = 1;
                break;
            case '?':
            case 'h':
            default:
                printf("Usage: %s device [-h] \n", argv[0]);
                printf("   \n");
                printf("       -v                   Verbose output \n");
                printf("       -h                   Print this message \n");
                printf("   \n");
                return -1;
        }
    }
    
    if (optind != argc - 1) {
        printf("Usage: %s device\n", argv[0]);
        return -1;
    }
    
    sisuser = malloc(sizeof(sis8300drv_usr));
    sisuser->file = argv[optind];

    status = sis8300drv_open_device(sisuser);
    if (status) {
        printf("sis8300drv_open_device error: %s (%d)\n", 
                sis8300drv_strerror(status), status);
        return -1;
    }
    
    status = sis8300drv_reg_read(sisuser, FW_VER_R_REG, &reg_value);
    if (status) {
        printf("sis8300drv_reg_read error: %s (%d)\n", 
                sis8300drv_strerror(status), status);
        return -1;
    }

    fw_patch =  reg_value & 0x000000ff;
    fw_minor = (reg_value & 0x00ffff00) >> 8;
    fw_major = (reg_value & 0xff000000) >> 24;

    
    if (verbose) {
        printf("fw version is %u.%u.%u\n", fw_major, fw_minor, fw_patch);
    }
    else {
        printf("%u.%u.%u\n", fw_major, fw_minor, fw_patch);
    }
    
    sis8300drv_close_device(sisuser);

	return(0);
}
